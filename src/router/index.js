import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

export default new VueRouter ( {
    mode: 'hash',
    routes: [
        {
            path: '/',
            component: require('../components/layout.vue').default,
            children: [
                {
                    path: '',
                    name: 'Index',
                    component: require('../pages/index.vue').default
                } 
            ]
        }
    ]
});